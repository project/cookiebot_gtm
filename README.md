CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

# Introduction

This module makes it possible for you to integrate [Cookiebot](https://www.cookiebot.com/)
and Google Tag Manager in a fast and simple way. You want this if you want
to prevent the placement of cookies and scripts implemented by GTM to run
before user agreement.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/cookiebot_gtm).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/cookiebot_gtm).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. Visit:
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules) for further information.


## Configuration

Configuration path `/admin/config/cookiebot_gtm` includes:

  1. **Cookiebot ID**: This is the unique ID for your Cookiebot account.
  You can find this ID in your Cookiebot account settings.
  2. **Consent type**: This option allows you to choose between explicit and
  implicit consent. Explicit consent means that visitors must actively
  opt-in to cookies, while implicit consent assumes consent unless the visitor
  opts-out.
  3. **Consent logging**: This option enables logging of user consent for
  auditing purposes. When enabled, Cookiebot will store a log of all user
  consents and updates to consents.
  4. **Banner position**: This option lets you choose the position of the
  cookie consent banner on your website, such as at the top or bottom of the
  page.
  5. **Banner style**: This option lets you choose the style of the cookie
  consent banner, such as color, font, and button text.
  6. **Banner language**: This option lets you choose the language of the
  cookie consent banner.
  7. **Cookie declaration**: This option enables a link to a cookie declaration
  page, which provides visitors with more information about the cookies used on
  your website. The cookie-declaration route can be found at
  **/cookie-declaration**.
  8. **Google Tag Manager ID**: This is the unique ID for your Google Tag
  Manager account. You can find this ID in your GTM account settings.

## Maintainers

- [immoreel](https://www.drupal.org/u/immoreel)
- Fabian de Rijk - [fabianderijk](https://www.drupal.org/u/fabianderijk)
- [Johan den Hollander](https://www.drupal.org/u/johan-den-hollander)
- Valerie Valkenburg-Gibson - [valgibson](https://www.drupal.org/u/valgibson)
- Robin Kroon - [robin.kroon](https://www.drupal.org/u/robinkroon)
