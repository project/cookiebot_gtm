<?php

namespace Drupal\cookiebot_gtm\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides configuration form for the Cookiebot GTM module.
 */
class CookiebotGtmConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Language\LanguageManagerInterface definition.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LanguageManagerInterface $languageManager) {
    parent::__construct($config_factory);
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cookiebot_gtm.cookiebot_gtm_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cookiebot_gtm_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cookiebot_gtm.cookiebot_gtm_config');
    $form['cbid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cookiebot Domain Group Id (CBID)'),
      '#description' => $this->t('This ID looks like 00000000-0000-0000-0000-000000000000. You can find it in the Cookiebot Manager on the &#039;Your scripts&#039; tab.'),
      '#maxlength' => 255,
      '#size' => 64,
      '#default_value' => $config->get('cbid'),
    ];

    $form['cookie_blocking'] = [
      '#type' => 'select',
      '#title' => $this->t('Cookie-blocking mode'),
      '#description' => $this->t('Use this to select the cookie blocking mode. Auto or manual'),
      '#default_value' => ($config->get('cookie_blocking')) ? $config->get('cookie_blocking') : 'auto',
      '#options' => [
        'auto' => $this->t('Auto (with script)'),
        'manual' => $this->t('Manual (without script)'),
      ],
    ];
    $form['use_multilingual'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use the multilingual Cookiebot'),
      '#description' => $this->t('When you enable this checkbox the current language is used for the Cookiebot banner.'),
      '#default_value' => $config->get('use_multilingual'),
    ];
    $form['use_multilingual_gtm_id'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use a different Google Tag Manager ID per language.'),
      '#default_value' => $config->get('use_multilingual_gtm_id'),
    ];
    $form['gtm_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Tag manager Container ID'),
      '#description' => $this->t('Your Google Tag Manager Container ID that looks like GTM-XXXXXXXX'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('gtm_id'),
      '#states' => [
        'visible' => [
          ':input[name="use_multilingual_gtm_id"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['gtm_hostname'] = [
      '#type' => 'url',
      '#title' => $this->t('Google Tag Manager Hostname.'),
      '#default_value' => $config->get('gtm_hostname'),
      '#placeholder' => 'https://www.googletagmanager.com',
      '#maxlength' => 255,
      '#size' => 80,
    ];
    $form['gtm_environment_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Tag Manager environment ID.'),
      '#default_value' => $config->get('gtm_environment_id'),
    ];
    $form['gtm_environment_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Tag Manager environment token.'),
      '#default_value' => $config->get('gtm_environment_token'),
    ];

    $form['languages'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Google Tag Manager Container ID per language'),
      '#states' => [
        'visible' => [
          ':input[name="use_multilingual_gtm_id"]' => ['checked' => TRUE],
        ],
      ],
    ];

    /** @var \Drupal\Core\Language\Language $language */
    foreach ($this->languageManager->getLanguages() as $language) {
      $title = $this->t('@lang Google Tag Manager Container ID', ['@lang' => $language->getName()]);
      $fieldName = 'gtm_id_' . $language->getId();

      $form['languages'][$fieldName] = [
        '#type' => 'textfield',
        '#title' => $title,
        '#description' => $this->t('Your @lang Google Tag Manager Container ID that looks like GTM-XXXXXXXX', ['@lang' => $language->getName()]),
        '#maxlength' => 64,
        '#size' => 64,
        '#default_value' => $config->get($fieldName),
      ];
    }

    $form['google_consent_mode_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Google Consent Mode settings'),
      '#open' => TRUE,
    ];

    $form['google_consent_mode_settings']['consent_mode_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Google Consent mode.'),
      '#default_value' => $config->get('consent_mode_enabled'),
    ];

    $form['google_consent_mode_settings']['ad_personalization'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grant access to Ad Personalization.'),
      '#default_value' => $config->get('ad_personalization'),
      '#states' => [
        'visible' => [
          ':input[name="consent_mode_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['google_consent_mode_settings']['ad_storage'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grant access to Ad Storage.'),
      '#default_value' => $config->get('ad_storage'),
      '#states' => [
        'visible' => [
          ':input[name="consent_mode_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['google_consent_mode_settings']['ad_user_data'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grant access to Ad User Data.'),
      '#default_value' => $config->get('ad_user_data'),
      '#states' => [
        'visible' => [
          ':input[name="consent_mode_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['google_consent_mode_settings']['analytics_storage'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grant access to Analytics Storage.'),
      '#default_value' => $config->get('analytics_storage'),
      '#states' => [
        'visible' => [
          ':input[name="consent_mode_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['google_consent_mode_settings']['functionality_storage'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grant access to Functionality Storage.'),
      '#default_value' => $config->get('functionality_storage'),
      '#states' => [
        'visible' => [
          ':input[name="consent_mode_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['google_consent_mode_settings']['personalization_storage'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Grant access to Personalization Storage.'),
      '#default_value' => $config->get('personalization_storage'),
      '#states' => [
        'visible' => [
          ':input[name="consent_mode_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $gtmId = $form_state->getValue('gtm_id');
    if (!preg_match('/^GTM-[A-Z0-9]{1,8}$/', $gtmId)) {
      $form_state->setErrorByName('gtm_id', $this->t('The entered container ID is not valid. The container ID needs to look like GTM-XXXXXXXX.'));
    }

    // Multilingual is enabled, so check those ID's as well.
    if (!empty($form_state->getValue('use_multilingual_gtm_id'))) {
      foreach ($this->languageManager->getLanguages() as $language) {
        $fieldName = 'gtm_id_' . $language->getId();
        $gtmId = $form_state->getValue($fieldName);
        if (!preg_match('/^GTM-[A-Z0-9]{1,8}$/', $gtmId)) {
          $form_state->setErrorByName($fieldName, $this->t('The entered @language container ID is not valid.', ['@language' => $language->getName()]));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('cookiebot_gtm.cookiebot_gtm_config');
    $config->set('cbid', $form_state->getValue('cbid'))
      ->set('cookie_blocking', $form_state->getValue('cookie_blocking'))
      ->set('use_multilingual', $form_state->getValue('use_multilingual'))
      ->set('use_multilingual_gtm_id', $form_state->getValue('use_multilingual_gtm_id'))
      ->set('consent_mode_enabled', $form_state->getValue('consent_mode_enabled'))
      ->set('ad_personalization', $form_state->getValue('ad_personalization'))
      ->set('ad_storage', $form_state->getValue('ad_storage'))
      ->set('ad_user_data', $form_state->getValue('ad_user_data'))
      ->set('analytics_storage', $form_state->getValue('analytics_storage'))
      ->set('functionality_storage', $form_state->getValue('functionality_storage'))
      ->set('personalization_storage', $form_state->getValue('personalization_storage'))
      ->set('gtm_id', $form_state->getValue('gtm_id'))
      ->set('gtm_hostname', $form_state->getValue('gtm_hostname'))
      ->set('gtm_environment_id', $form_state->getValue('gtm_environment_id'))
      ->set('gtm_environment_token', $form_state->getValue('gtm_environment_token'));

    /** @var \Drupal\Core\Language\Language $language */
    foreach ($this->languageManager->getLanguages() as $language) {
      $config->set('gtm_id_' . $language->getId(), $form_state->getValue('gtm_id_' . $language->getId()));
    }

    $config->save();
  }

}
