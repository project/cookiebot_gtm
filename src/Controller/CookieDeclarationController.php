<?php

namespace Drupal\cookiebot_gtm\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides controller for the cookie declaration.
 */
class CookieDeclarationController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configFactory = $container->get('config.factory');
    $instance->languageManager = $container->get('language_manager');
    return $instance;
  }

  /**
   * Show page.
   *
   * @return array
   *   The render array.
   */
  public function showPage() {
    $config = $this->configFactory->get('cookiebot_gtm.cookiebot_gtm_config');
    $multilingual = $config->get('use_multilingual');
    $language = FALSE;

    if ($multilingual) {
      $language = $this->languageManager->getCurrentLanguage()->getId();
    }

    return [
      '#theme' => 'cookiebot_gtm_declaration',
      '#cbid' => $config->get('cbid'),
      '#language' => $language,
    ];
  }

}
